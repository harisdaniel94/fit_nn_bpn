#pragma once
#include <vector>
#include <tuple>
#include <map>
#include "NeuralNetwork.h"

using namespace std;

namespace NN {

    /**
     * http://www.dataschool.io/simple-guide-to-confusion-matrix-terminology/
     */
    class ConfusionMatrix {

    public:

        // constructor
        ConfusionMatrix(NeuralNetwork*);

        // destructor
        ~ConfusionMatrix();

        /**
         * Method sets the 4 variables of confusion matrix:
         * - true positives
         * - true negatives
         * - false positives
         * - false negatives
         */
        void measure(vector<tuple<vector<double>,vector<double>>>);

        /**
         * Method computes various performance stats about the classifier
         */
        void computeNetworkPerformance();

        /**
         * Method prints the correlation matrix
         */
        void print();

    public:
        NeuralNetwork *nn;
        int totalSamples;
        /* cases in which we predicted YES and desired output is YES */
        int truePositives;
        /* cases in which we predicted NO and desired output is NO */
        int trueNegatives;
        /* cases in which we predicted YES but desired output is NO */
        int falsePositives;
        /* cases in which we predicted NO and desired output is YES */
        int falseNegatives;
        /* ACCURACY: Overall, how often is the classifier correct? */
        double accuracy;
        /* MISCLASSIFICATION RATE aka ERROR RATE: Overall, how often is it wrong? */
        double misclassificationRate;
        /* TRUE POSITIVE RATE aka SENSITIVITY/RECALL: When it's actually yes, how often does it predict yes? */
        double truePositiveRate;
        /* FALSE POSITIVE RATE: When it's actually no, how often does it predict yes? */
        double falsePositiveRate;
        /* SPECIFICITY: When it's actually no, how often does it predict no? */
        double specificity;
        /* PRECISION: When it predicts yes, how often is it correct? */
        double precision;
        /* PREVALENCE: How often does the yes condition actually occur in our sample? */
        double prevalence;
        /* how often you would be wrong if you always predicted the majority class */
        double nullErrorRate;
        /* NN will have high Kappa score if there is a big difference between the accuracy and the null error rate */
        double cohenKappaScore;
        /* F-SCORE: Weighted average of the true positive rate (recall) and precision */
        double fScore;
    };

    /**
     * Structure for best model and it's stats
     */
    struct bestModelStats {
        NeuralNetwork *bestModel;
        double averageTrainingError;
        int hiddenDimension;
        double learningRate;
        double momentum;
        double weightInitInterval;
        ConfusionMatrix *confusionMatrix;
    };

    class GridSearch {

    public:

        // constructor
        GridSearch(vector<tuple<vector<double>,vector<double>>>, vector<int>, vector<double>, vector<double>,
                   vector<double>, double);

        // destructor
        ~GridSearch();

        /**
         * Method performs the grid search over hyper-parameter space
         * returns a structure of best model and its stats
         */
        bestModelStats getBestModel();

        /**
         * Method splits the data-set into 2 parts by a given split rate - training and testing data
         */
        map<string,vector<tuple<vector<double>,vector<double>>>> trainTestSplit();

    public:
        double trainTestSplitRate;
        vector<tuple<vector<double>,vector<double>>> samples;
        vector<int> hiddenDimensions;
        vector<double> weightInitIntervals;
        vector<double> learningRates;
        vector<double> momentums;
    };

}
