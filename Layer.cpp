#include <iostream>
#include <vector>
#include <random>
#include "Layer.h"

using namespace std;

namespace NN {

    Layer::Layer(
            int neuronsCount,
            int prevLayerNeuronsCount,
            double weightInitInterval,
            double learningRate,
            double momentum,
            bool last) {

        random_device rd;
        mt19937 gen(rd());
        uniform_real_distribution<double> fixedDistribution(-weightInitInterval, weightInitInterval);
        uniform_real_distribution<double> variableDistribution(-(3.0/sqrt(neuronsCount)), (3.0/sqrt(neuronsCount)));
        this->bias = (weightInitInterval) ? fixedDistribution(gen) : variableDistribution(gen);
        this->last = last;
        this->learningRate = learningRate;
        this->momentum = momentum;
        // create neurons
        for (int i = 0; i < neuronsCount; i++)
            this->neurons.push_back(new Neuron(this->bias));
        // initialize weights of neurons
        for (auto neuron: this->neurons) {
            for (int i = 0; i < prevLayerNeuronsCount; i++) {
                neuron->weights.push_back((weightInitInterval) ? fixedDistribution(gen) : variableDistribution(gen));
                neuron->prevWeightChanges.push_back(0.0);
            }
        }
    }

    vector<double> Layer::feedForward(vector<double> inputs) {
        vector<double> outputs;
        for (auto neuron: this->neurons)
            outputs.push_back(neuron->calculateOutput(inputs));
        return outputs;
    }

    void Layer::calculateDeltas(vector<double> trainingOutputs) {
        for (unsigned int i = 0; i < this->neurons.size(); i++)
            // ∂E/∂uⱼ
            this->neurons[i]->delta = this->neurons[i]->calculatePdErrorWrtTotalNetInput(trainingOutputs[i]);
    }

    void Layer::calculateDeltas(Layer *nextLayer) {
        for (unsigned int i = 0; i < this->neurons.size(); i++) {
            /* We need to calculate the derivative of the error
             * with respect to the output of each hidden layer neuron
             * dE/dyⱼ = Σ ∂E/∂uⱼ * ∂z/∂yⱼ = Σ ∂E/∂uⱼ * wᵢⱼ */
            double weightedDeltaSum = 0.0;
            for (unsigned int k = 0; k < nextLayer->neurons.size(); k++)
                weightedDeltaSum += nextLayer->neurons[k]->delta * nextLayer->neurons[k]->weights[i];
            /* ∂E/∂uⱼ = dE/dyⱼ * ∂uⱼ/∂ */
            this->neurons[i]->delta = weightedDeltaSum * this->neurons[i]->calculatePdTotalNetInputWrtInput();
        }
    }

    void Layer::updateWeights() {
        for (unsigned int j = 0; j < this->neurons.size(); j++) {
            for (unsigned int i = 0; i < this->neurons[j]->weights.size(); i++) {
                // ∂Eⱼ/∂wᵢ = α * ∂E/∂uⱼ * ∂uⱼ/∂wᵢⱼ
                double weightChange = this->learningRate
                                      * this->neurons[j]->delta
                                      * this->neurons[j]->calculatePdTotalNetInputWrtWeight(i);
                if (this->momentum) {
                    // m * Δw_old
                    double momentumChange = this->momentum * this->neurons[j]->prevWeightChanges[i];
                    /* Δw = α * ∂Eⱼ/∂wᵢ = α * ∂E/∂uⱼ * ∂uⱼ/∂wᵢⱼ + */
                    this->neurons[j]->weights[i] -= weightChange + momentumChange;
                    this->neurons[j]->prevWeightChanges[i] = weightChange + momentumChange;
                } else {
                    this->neurons[j]->weights[i] -= weightChange;
                }
            }
        }
    }

    void Layer::print() {
        // print all neurons of current layer
        cout << "Neurons: " << this->neurons.size() << endl;
        for (unsigned int i = 0; i < this->neurons.size(); i++) {
            cout << "\tNeuron #" << i << endl;
            for (unsigned int j = 0; j < this->neurons[i]->weights.size(); j++)
                cout << "\t\tWeight #" << j << ": " << this->neurons[i]->weights[j] << endl;
            cout << "\t\tBias: " << this->neurons[i]->bias << endl;
        }
    }

    Layer::~Layer() {
        // delete all neurons from layer
        for (auto neuron: neurons) delete neuron;
    }

}
