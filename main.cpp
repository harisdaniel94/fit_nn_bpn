#include <iostream>
#include <random>
#include <map>
#include "NeuralNetwork.h"
#include "csv.h"
#include "Stats.h"

using namespace io;
using namespace std;
using namespace NN;

const string TITANIC_DATA_PATH = "titanic_scaled.csv";
const string MUSHROOMS_DATA_PATH = "mushrooms_scaled.csv";

/**
 * Function handles titanic data set
 */
vector<tuple<vector<double>, vector<double>>> getTitanicSamples() {
    vector<tuple<vector<double>, vector<double>>> samples;
    double ticket_class, survived, siblings, parents, age, sex, fare;
    CSVReader<7, trim_chars<' '>, no_quote_escape<';'>> in(TITANIC_DATA_PATH);
    in.read_header(ignore_no_column, "ticket_class", "survived", "sex", "age", "siblings", "parents", "fare");
    while (in.read_row(ticket_class, survived, sex, age, siblings, parents, fare)) {
        vector<double> features {ticket_class, sex, age, siblings, parents, fare};
        vector<double> target {survived};
        tuple<vector<double>, vector<double>> sample {features, target};
        samples.push_back(sample);
    }
    return samples;
}

/**
 * Function handles original mushrooms data set
 */
vector<tuple<vector<double>, vector<double>>> getMushroomSamples() {
    vector<tuple<vector<double>, vector<double>>> samples;
    double label, cap_shape, cap_surface, cap_color, bruises, odor, gill_attachment, gill_spacing, gill_size, gill_color,
            stalk_shape, stalk_root, stalk_surface_above_ring, stalk_surface_below_ring, stalk_color_above_ring,
            stalk_color_below_ring, veil_type, veil_color, ring_number, ring_type, spore_print_color, population,
            habitat;
    CSVReader<23, trim_chars<' '>, no_quote_escape<';'>> in(MUSHROOMS_DATA_PATH);
    in.read_header(ignore_no_column, "label", "cap_shape", "cap_surface", "cap_color", "bruises", "odor", "gill_attachment",
                   "gill_spacing", "gill_size", "gill_color", "stalk_shape", "stalk_root", "stalk_surface_above_ring",
                   "stalk_surface_below_ring", "stalk_color_above_ring", "stalk_color_below_ring", "veil_type", "veil_color",
                   "ring_number", "ring_type", "spore_print_color", "population", "habitat");
    while (in.read_row(label, cap_shape, cap_surface, cap_color, bruises, odor, gill_attachment, gill_spacing,
                       gill_size, gill_color, stalk_shape, stalk_root, stalk_surface_above_ring, stalk_surface_below_ring,
                       stalk_color_above_ring, stalk_color_below_ring, veil_type, veil_color, ring_number, ring_type,
                       spore_print_color, population, habitat)) {
        vector<double> features{
                cap_shape, cap_surface, cap_color, bruises, odor, gill_attachment, gill_spacing, gill_size,
                gill_color,
                stalk_shape, stalk_root, stalk_surface_above_ring, stalk_surface_below_ring, stalk_color_above_ring,
                stalk_color_below_ring, veil_type, veil_color, ring_number, ring_type, spore_print_color,
                population,
                habitat};
        vector<double> target{label};
        tuple<vector<double>, vector<double>> sample{features, target};
        samples.push_back(sample);
    }
    return samples;
}

/**
 * Function handles preprocessed mushrooms data set
 */
vector<tuple<vector<double>, vector<double>>> getPreprocessedMushroomSamples() {
    vector<tuple<vector<double>, vector<double>>> samples;
    double label, cap_shape, cap_surface, cap_color, bruises, odor, gill_attachment, gill_spacing, gill_size, gill_color,
            stalk_shape, stalk_root, stalk_surface_above_ring, stalk_surface_below_ring, stalk_color_above_ring,
            stalk_color_below_ring, veil_type, veil_color, ring_number, ring_type, spore_print_color, population,
            habitat;
    CSVReader<23, trim_chars<' '>, no_quote_escape<';'>> in(MUSHROOMS_DATA_PATH);
    in.read_header(ignore_no_column, "label", "cap_shape", "cap_surface", "cap_color", "bruises", "odor", "gill_attachment",
                   "gill_spacing", "gill_size", "gill_color", "stalk_shape", "stalk_root", "stalk_surface_above_ring",
                   "stalk_surface_below_ring", "stalk_color_above_ring", "stalk_color_below_ring", "veil_type", "veil_color",
                   "ring_number", "ring_type", "spore_print_color", "population", "habitat");
    while (in.read_row(label, cap_shape, cap_surface, cap_color, bruises, odor, gill_attachment, gill_spacing,
                       gill_size, gill_color, stalk_shape, stalk_root, stalk_surface_above_ring, stalk_surface_below_ring,
                       stalk_color_above_ring, stalk_color_below_ring, veil_type, veil_color, ring_number, ring_type,
                       spore_print_color, population, habitat)) {
        vector<double> features{
                cap_shape, cap_surface, odor, gill_color, stalk_surface_above_ring, stalk_color_above_ring, veil_color,
                ring_number, population, habitat};
        vector<double> target{label};
        tuple<vector<double>, vector<double>> sample{features, target};
        samples.push_back(sample);
    }
    return samples;
}

/**
 * Function prints out content of bestModelStats structure
 */
void printBestModel(bestModelStats bms) {
    cout << "+-------------------------+" << endl;
    cout << "|       Best Network      |" << endl;
    cout << "+-------------------------+" << endl;
    cout << "|           Hidden Neurons: " << bms.hiddenDimension << endl;
    cout << "|     Total Training Error: " << bms.averageTrainingError << endl;
    cout << "|          Weight Interval: ";
    if (bms.weightInitInterval) {
        cout << "fixed interval ( -" << bms.weightInitInterval << ", " << bms.weightInitInterval << " )" << endl;
    } else {
        cout << "(-(3/sqrt(n)), +(3/sqrt(n)))" << endl;
    }
    cout << "|            Learning Rate: " << bms.learningRate << endl;
    cout << "|                 Momentum: " << bms.momentum << endl;
    cout << "+-------------------------+" << endl;
    bms.confusionMatrix->print();
}

void wrongArguments() {
    cerr << "Wrong arguments" << endl;
    cerr << "\nUSAGE: ./bpn {-o|--original, -s|--selected} [OPTIONS]" << endl;
    cerr << "OPTIONS:" << endl;
    cerr << "  -h|--hidden H           - hidden neurons count (1 < H < 100)" << endl;
    cerr << "  -l|--learning-rate L    - learning rate (0.1 <= L <= 0.9)" << endl;
    cerr << "  -m|--momentum M         - momentum term (0.5 <= M <= 1.0)" << endl;
    cerr << "  -w|--weight-interval W  - initial weight interval" << endl;
}

bool isFloat(const std::string& s) {
    try {
        std::stof(s);
        return true;
    } catch(...) {
        return false;
    }
}

int main(int argc, char *argv[]) {
    vector<tuple<vector<double>, vector<double>>> samples;
    int hiddenNeuronsCount = 0;
    double learningRate = 0.0, momentum = 0.0, weightInterval = 0.0;
    double trainTestSplitRate = 0.75;
    vector<int> hiddenNeuronCounts;
    vector<double> learningRates;
    vector<double> momentums;
    vector<double> weightInitIntervals;
    // load training data set
    if (argc == 2) {
        if (strcmp(argv[1], "--original") || strcmp(argv[1], "-o"))
            samples = getMushroomSamples();
        else if (strcmp(argv[1], "--selected") || strcmp(argv[1], "-s"))
            samples = getPreprocessedMushroomSamples();
        else {
            wrongArguments();
            return 1;
        }
    } else if (argc == 10) {
        if (!(strcmp(argv[1], "--original") || strcmp(argv[1], "-o") || strcmp(argv[1], "--selected") || strcmp(argv[1], "-s")) ||
            !((strcmp(argv[2], "--hidden") || strcmp(argv[2], "-h")) && isFloat(argv[3])) ||
            !((strcmp(argv[4], "--learning-rate") || strcmp(argv[4], "-l")) && isFloat(argv[5])) ||
            !((strcmp(argv[6], "--momentum") || strcmp(argv[6], "-m")) && isFloat(argv[7])) ||
            !((strcmp(argv[8], "--weight-interval") || strcmp(argv[8], "-w")) && isFloat(argv[9]))) {
            wrongArguments();
            return 1;
        }
        samples = (strcmp(argv[1], "--original") || strcmp(argv[1], "-o")) ? getMushroomSamples() : getPreprocessedMushroomSamples();
        hiddenNeuronsCount = stoi(argv[3]);
        learningRate = stod(argv[5]);
        momentum = stod(argv[7]);
        weightInterval = stod(argv[9]);
    } else {
        wrongArguments();
        return 1;
    }
    if (argc == 2) {
        /* create hyper-parameter space */
        hiddenNeuronCounts.insert(hiddenNeuronCounts.end(), {3, 5, 9, 11, 15});
        learningRates.insert(learningRates.end(), {0.1, 0.3, 0.5, 0.7, 0.9});
        momentums.insert(momentums.end(), {0.0, 0.5, 0.7, 0.9, 0.95});
        weightInitIntervals.insert(weightInitIntervals.end(), {0.0, 0.5});
    } else {
        hiddenNeuronCounts.push_back(hiddenNeuronsCount);
        learningRates.push_back(learningRate);
        momentums.push_back(momentum);
        weightInitIntervals.push_back(weightInterval);
    }
    /*
     * performs grid search with simple train test split in given split rate
     * returns structure with best model and its stats
     */
    GridSearch gs(samples, hiddenNeuronCounts, learningRates, momentums, weightInitIntervals, trainTestSplitRate);
    bestModelStats bms = gs.getBestModel();
    printBestModel(bms);
    // release
    delete bms.bestModel;
    delete bms.confusionMatrix;
}
