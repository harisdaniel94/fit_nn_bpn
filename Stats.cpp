#include <vector>
#include <tuple>
#include <map>
#include <algorithm>
#include <climits>
#include "Stats.h"

using namespace std;

namespace NN {

    /* Grid Search */
    GridSearch::GridSearch(
            vector<tuple<vector<double>, vector<double>>> samples,
            vector<int> hiddenDimensions,
            vector<double> learningRates,
            vector<double> momentums,
            vector<double> weightInitIntervals,
            double trainTestSplitRate) {
        this->samples = samples;
        this->hiddenDimensions = hiddenDimensions;
        this->weightInitIntervals = weightInitIntervals;
        this->learningRates = learningRates;
        this->momentums = momentums;
        this->trainTestSplitRate = trainTestSplitRate;
    }

    map<string,vector<tuple<vector<double>,vector<double>>>> GridSearch::trainTestSplit() {
        random_shuffle(this->samples.begin(), this->samples.end());  // perform random shuffle
        int splitIndex = (int)(this->samples.size() * this->trainTestSplitRate);  // get split index wrt train test split rate
        map<string,vector<tuple<vector<double>,vector<double>>>> result;
        vector<tuple<vector<double>,vector<double>>> trainingData(this->samples.begin(), this->samples.begin() + splitIndex);
        vector<tuple<vector<double>,vector<double>>> testingData(this->samples.begin() + splitIndex, this->samples.end());
        result["train"] = trainingData;
        result["test"] = testingData;
        return result;
    }

    bestModelStats GridSearch::getBestModel() {
        // total error for actual model, best error - initialized to MAX value
        double totalTrainingError = 0.0;
        double bestTrainingError = INT_MAX;
        bestModelStats bms{};  // structure for information about the best model
        // number of input/output neurons
        int inputDimension = get<0>(this->samples[0]).size();
        int outputDimension = get<1>(this->samples[0]).size();
        for (auto hiddenDimension: this->hiddenDimensions) {  // for all hidden dimensions
            for (auto weightInitInterval: this->weightInitIntervals) {  // for all init weight intervals
                for (auto learningRate: this->learningRates) {  // for all learning rates
                    for (auto momentum: this->momentums) {
                        printf("H=%d | W=%.2f | L=%.2f | M=%.2f\n", hiddenDimension, weightInitInterval, learningRate, momentum);
                        // create neural net with current combination of parameters
                        auto *nn = new NeuralNetwork(inputDimension, hiddenDimension, outputDimension, learningRate,
                                                     momentum, weightInitInterval);
                        // get results of cross validation
                        map<string, vector<tuple<vector<double>, vector<double>>>> data = this->trainTestSplit();
                        for (int i = 1; i <= 100; i++) {
                            nn->train(data["train"]);
                            // totalTrainingError = nn->calculateTotalError(data["train"]);
                            // printf("\r[%d] training_error: %.2f", i, totalTrainingError);
                            // cout << flush;
                        }
                        // cout << endl;
                        // compute confusion matrix
                        ConfusionMatrix *cm = new ConfusionMatrix(nn);
                        cm->measure(data["test"]);
                        cm->computeNetworkPerformance();
                        // best model == model with lowest average error
                        if (totalTrainingError < bestTrainingError) {
                            bestTrainingError = totalTrainingError;
                            bms.bestModel = nn;
                            bms.averageTrainingError = bestTrainingError;
                            bms.learningRate = learningRate;
                            bms.momentum = momentum;
                            bms.hiddenDimension = hiddenDimension;
                            bms.weightInitInterval = weightInitInterval;
                            bms.confusionMatrix = cm;
                        } else {  // if model not better than current best, delete model and continue
                            delete nn;
                            delete cm;
                        }
                    }
                }
            }
        }
        return bms;
    }

    GridSearch::~GridSearch() { }

    /* Confusion Matrix */

    ConfusionMatrix::ConfusionMatrix(NeuralNetwork *nn) {
        this->nn = nn;
        this->totalSamples = 0;
        this->truePositives = 0;
        this->trueNegatives = 0;
        this->falsePositives = 0;
        this->falseNegatives = 0;
        this->accuracy = 0.0;
        this->misclassificationRate = 0.0;
        this->truePositiveRate = 0.0;
        this->falsePositiveRate = 0.0;
        this->specificity = 0.0;
        this->precision = 0.0;
        this->prevalence = 0.0;
        this->nullErrorRate = 0.0;
        this->cohenKappaScore = 0.0;
        this->fScore = 0.0;
    }

    void ConfusionMatrix::measure(vector<tuple<vector<double>,vector<double>>> data) {
        this->totalSamples = data.size();  // set total samples count
        for (auto sample: data) {
            vector<double> inputVector = get<0>(sample);
            vector<double> outputVector = get<1>(sample);
            nn->feedForward(inputVector);
            if (nn->outputLayer->neurons[0]->output > 0.5 && outputVector[0] == 1)
                this->truePositives++;
            else if (nn->outputLayer->neurons[0]->output > 0.5 && outputVector[0] == 0)
                this->falsePositives++;
            else if (nn->outputLayer->neurons[0]->output <= 0.5 && outputVector[0] == 1)
                this->falseNegatives++;
            else if (nn->outputLayer->neurons[0]->output <= 0.5 && outputVector[0] == 0)
                this->trueNegatives++;
        }
    }

    void ConfusionMatrix::computeNetworkPerformance() {
        /* ACCURACY = (TP + TN) / total */
        (totalSamples) ? accuracy = ((double)truePositives + trueNegatives) / totalSamples : -1.0;
        /* MISCLASSIFICATION RATE = (FP + FN) / total OR (1 - accuracy) */
        (totalSamples) ? misclassificationRate = ((double)falsePositives + falseNegatives) / totalSamples : -1.0;
        /* TRUE POSITIVE RATE = TP / (actual true) */
        ((truePositives + falseNegatives) > 0) ? truePositiveRate = truePositives / (double)(truePositives + falseNegatives) : -1.0;
        /* FALSE POSITIVE RATE = FP / (actual false) */
        ((falsePositives + trueNegatives) > 0) ? falsePositiveRate = falsePositives / (double)(falsePositives + trueNegatives): -1.0;
        /* SPECIFICITY = TN / (actual false) OR (1 - false positive rate) */
        ((falsePositives + trueNegatives) > 0) ? specificity = trueNegatives / (double)(falsePositives + trueNegatives) : -1.0;
        /* PRECISION = TP / (predicted true) */
        ((truePositives + falsePositives) > 0) ? precision = truePositives / (double)(truePositives + falsePositives) : -1.0;
        /* PREVALENCE = (actual true) / total */
        (totalSamples) ? prevalence = ((double)truePositives + falseNegatives) / totalSamples : -1.0;
        /* NULL ERROR RATE */
        (totalSamples) ? nullErrorRate = ((double)falsePositives + trueNegatives) / totalSamples : -1.0;
        /* cohen kappa score */
        double p_true = (((double)truePositives + falseNegatives) / totalSamples)
                        * (((double)truePositives + falsePositives) / totalSamples);
        double p_false = (((double)falsePositives + trueNegatives) / totalSamples)
                         * (((double)falseNegatives + trueNegatives) / totalSamples);
        double p_e = p_true + p_false;
        cohenKappaScore = (p_e <= 1) ? (accuracy - p_e) / (1.0 - p_e) : -1.0;
        /* F(1) Score = 2 * (precision * truePositiveRate) / (precision + truePositiveRate) */
        fScore = ((precision + truePositiveRate) > 0) ? 2 * ((precision * truePositiveRate) / (precision + truePositiveRate)) : -1.0;
    }

    int num_len(int x) {
        int s = 0;
        if (x == 0) { s = 1; } else { while(x != 0) { ++s; x /= 10; }}
        return s;
    }

    void ConfusionMatrix::print() {
        string tps = "", fns = "", fps = "", tns = "";
        for (int i = 0; i < (5-num_len(truePositives)); i++) tps += " ";
        for (int i = 0; i < (5-num_len(falseNegatives)); i++) fns += " ";
        for (int i = 0; i < (5-num_len(falsePositives)); i++) fps += " ";
        for (int i = 0; i < (5-num_len(trueNegatives)); i++) tns += " ";
        cout << "                    +---------------+" << endl;
        cout << "                    |     Actual    |" << endl;
        cout << "                    +-------+-------+" << endl;
        cout << "                    |  True | False |" << endl;
        cout << "+-----------+-------+-------+-------+" << endl;
        printf("|           |  True | %s%d | %s%d |\n", tps.c_str(), truePositives, fns.c_str(), falseNegatives);
        cout << "| Predicted +-------+-------+-------+" << endl;
        printf("|           | False | %s%d | %s%d |\n", fps.c_str(), falsePositives, tns.c_str(), trueNegatives);
        cout << "+-----------+-------+-------+-------+" << endl << endl;
        cout << "+-------------------------+" << endl;
        printf("|                 Accuracy: %.2f %%\n", accuracy * 100);
        printf("|   Misclassification Rate: %.2f %%\n", misclassificationRate * 100);
        printf("|       True Positive Rate: %.2f %%\n", truePositiveRate * 100);
        printf("|      False Positive Rate: %.2f %%\n", falsePositiveRate * 100);
        printf("|              Specificity: %.2f %%\n", specificity * 100);
        printf("|                Precision: %.2f %%\n", precision * 100);
        printf("|               Prevalence: %.2f %%\n", prevalence * 100);
        printf("|          Null Error Rate: %.2f %%\n", nullErrorRate * 100);
        printf("|        Cohen Kappa Score: %.2f %%\n", cohenKappaScore * 100);
        printf("|                  F Score: %.2f %%\n", fScore * 100);
        cout << "+-------------------------+" << endl;

    }

    ConfusionMatrix::~ConfusionMatrix() { }
}
