# import csv
import numpy as np
import pandas as pd
# import matplotlib.pyplot as plt
# import seaborn as sns
from string import ascii_letters
from sklearn.preprocessing import MinMaxScaler
# rcParams.update({'figure.autolayout': True})
# sns.set(style="white")

def normalize_data(df):
	scaler = MinMaxScaler()
	df_scaled = pd.DataFrame(scaler.fit_transform(df), columns=df.columns, index=df.index)
	df_scaled.to_csv("mushrooms_scaled.csv", sep=";", index=False)
	return df_scaled

# def save_correlation_matrix(df, figure_name, remove_cols=None):
# 	if remove_cols is None:
# 		figure_name += "original_features.png"
# 	else:
# 		figure_name += "feature_selection.png"
# 		for col in remove_cols:
# 			del df[col]
# 	corr = df.corr()  # Compute the correlation matrix
# 	mask = np.zeros_like(corr, dtype=np.bool)  # Generate a mask for the upper triangle
# 	mask[np.triu_indices_from(mask)] = True
# 	# Set up the matplotlib figure
# 	f, ax = plt.subplots(figsize=(11, 9))
# 	# Generate a custom diverging colormap
# 	cmap = sns.diverging_palette(220, 10, as_cmap=True)
# 	# Draw the heatmap with the mask and correct aspect ratio
# 	sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.5, center=0,
# 	            square=True, linewidths=.5, cbar_kws={"shrink": .5})
# 	plt.savefig(figure_name)

if __name__ == "__main__":
	df = normalize_data(pd.read_csv("mushrooms.csv", sep=","))
	# save_correlation_matrix(df, "mushrooms_correlation_matrix", remove_cols=False)
