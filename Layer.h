#pragma once

#include <numeric>
#include <vector>
#include "Neuron.h"
#include <math.h>

using namespace std;

namespace NN {

    class Layer {

    public:
        // constructor
        Layer(int, int, double, double, double, bool);

        // destructor
        ~Layer();

        /**
         * Method computes outputs of all neurons in current layer
         */
        vector<double> feedForward(vector<double>);

        /**
         * Method
         */
        void calculateDeltas(Layer*);

        /**
         * Method
         */
        void calculateDeltas(vector<double>);

        /**
         * Method
         */
        void updateWeights();

        /**
         * Method prints all neurons for the current layer to stdout
         */
        void print();

    public:
        bool last;
        double bias;
        double learningRate;
        double momentum;
        vector<Neuron*> neurons;

    };

}
