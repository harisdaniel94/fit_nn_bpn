#pragma once
#include <vector>

using namespace std;

namespace NN {

    class Neuron {

    public:
        // constructors
        Neuron();
        Neuron(double);

        // destructors
        ~Neuron();

        /**
         * Method computes yⱼ = activation(uⱼ)
         */
        double calculateOutput(vector<double>);

        /**
         * Method computes Eⱼ = 0.5*(dⱼ - yⱼ)^2
         */
        double calculateError(double);

        /**
         * Method computes ∂E/∂uⱼ (delta)
         * dE/dyⱼ = Σ ∂E/∂uⱼ * ∂uⱼ/∂yⱼ = Σ ∂E/∂uⱼ * wᵢⱼ
         * ∂E/∂uⱼ = dE/dyⱼ * ∂uⱼ/∂
         */
        double calculatePdErrorWrtTotalNetInput(double);

        /**
         * Method computes ∂Eⱼ/∂yⱼ = -(oⱼ - yⱼ)
         */
        double calculatePdErrorWrtOutput(double);

        /**
         * Method computes ∂yⱼ/∂uⱼ = yⱼ * (1 - yⱼ)
         */
        double calculatePdTotalNetInputWrtInput();

        /**
         * Method computes ∂uⱼ/∂wⱼᵢ = xᵢ
         */
        double calculatePdTotalNetInputWrtWeight(int);

        /**
         * Method prints neuron's input, weights and bias to stdout
         */
        void print();

    public:
        double bias;
        double output;
        double totalNetInput;
        double delta;
        vector<double> inputs;
        vector<double> weights;
        vector<double> prevWeightChanges;

    };

}
