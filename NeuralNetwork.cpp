#include <vector>
#include <tuple>
#include <map>
#include <algorithm>
#include <math.h>
#include <random>
#include "NeuralNetwork.h"
#include "Neuron.h"
#include "Layer.h"

using namespace std;

namespace NN {

    NeuralNetwork::NeuralNetwork(
            int inputCount,
            int hiddenCount,
            int outputCount,
            double learningRate,
            double momentum,
            double weightInitInterval) {

        this->inputCount = inputCount;
        this->learningRate = learningRate;
        this->momentum = momentum;

        /* initialize input layer */
        this->inputLayer = new Layer(inputCount, inputCount, weightInitInterval, learningRate, momentum, false);
        this->layers.push_back(this->inputLayer);

        /* initialize hidden layer */
        this->hiddenLayer = new Layer(hiddenCount, inputCount, weightInitInterval, learningRate, momentum, false);
        this->layers.push_back(this->hiddenLayer);

        /* initialize output layer */
        this->outputLayer = new Layer(outputCount, hiddenCount, weightInitInterval, learningRate, momentum, true);
        this->layers.push_back(this->outputLayer);
    }

    vector<double> NeuralNetwork::feedForward(vector<double> inputs) {
        vector<double> inputLayerOutputs = this->inputLayer->feedForward(inputs);
        vector<double> hiddenLayerOutputs = this->hiddenLayer->feedForward(inputLayerOutputs);
        return this->outputLayer->feedForward(hiddenLayerOutputs);
    }

    void NeuralNetwork::train(vector<tuple<vector<double>, vector<double>>> trainingSamples) {

        // perform a random shuffle of training samples
        random_shuffle(trainingSamples.begin(), trainingSamples.end());

        // for all training samples
        for (auto trainingSample: trainingSamples) {
            vector<double> trainingInputs = get<0>(trainingSample);
            vector<double> trainingOutputs = get<1>(trainingSample);

            /* Compute outputs */
            this->feedForward(trainingInputs);

            /* Compute deltas */
            for (int l = this->layers.size()-1; l >= 0; l--)
                (this->layers[l]->last) ? this->layers[l]->calculateDeltas(trainingOutputs)
                                        : this->layers[l]->calculateDeltas(this->layers[l+1]);

            /* Update weights */
            for (auto layer: this->layers)
                layer->updateWeights();
        }

    }

    double NeuralNetwork::calculateTotalError(vector<tuple<vector<double>, vector<double>>> samples) {
        double totalError = 0.0;
        for (auto sample: samples) {
            vector<double> inputVector = get<0>(sample);
            vector<double> outputVector = get<1>(sample);
            this->feedForward(inputVector);
            for (unsigned int i = 0; i < outputVector.size(); i++)
                totalError += this->outputLayer->neurons[i]->calculateError(outputVector[i]);
        }
        return totalError;
    }

    NeuralNetwork::~NeuralNetwork() {
        // delete all layers
        delete this->inputLayer;
        delete this->hiddenLayer;
        delete this->outputLayer;
    }

    void NeuralNetwork::print() {
        cout << "\n*******************" << endl;
        cout << ">> Inputs " << this->inputCount << " <<";
        cout << "\n>> InputLayer(" << inputLayer->neurons.size() << ") <<\n";
        hiddenLayer->print();
        cout << "\n>> HiddenLayer(" << hiddenLayer->neurons.size() << ") <<\n";
        hiddenLayer->print();
        cout << "\n>> OutputLayer(" << outputLayer->neurons.size() << ") <<\n";
        outputLayer->print();
    }

}
