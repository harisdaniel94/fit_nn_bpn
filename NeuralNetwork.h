#pragma once
#include <stdint.h>
#include <iostream>
#include <vector>
#include <tuple>
#include "Neuron.h"
#include "Layer.h"

using namespace std;

namespace NN {

    class NeuralNetwork {

    public:
        // constructor
        NeuralNetwork();
        NeuralNetwork(int, int, int, double, double, double);

        // destructor
        ~NeuralNetwork();

        /**
         * Method computes the output vector for the given input vector
         */
        vector<double> feedForward(vector<double>);

        /**
         * Method performs back-propagation learning process for given training samples
         */
        void train(vector<tuple<vector<double>, vector<double>>>);

        /**
         * Method calculates total error for given inputs
         */
        double calculateTotalError(vector<tuple<vector<double>, vector<double>>>);

        /**
         * Method prints neural network to stdout
         */
        void print();

    public:
        double inputCount;
        double learningRate;
        double momentum;

        Layer *inputLayer;
        Layer *hiddenLayer;
        Layer *outputLayer;
        vector<Layer*> layers;
    };

}
