CC=g++
FLAGS=-std=gnu++11 -Wall -pedantic -pthread

SRCS=NeuralNetwork.cpp Neuron.cpp Layer.cpp Stats.cpp main.cpp

all:
	$(CC) $(FLAGS) $(SRCS) -o bpn

clean:
	rm -f *.o
