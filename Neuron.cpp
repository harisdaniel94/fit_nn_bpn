#include <iostream>
#include <vector>
#include <numeric>
#include <math.h>
#include "Neuron.h"

using namespace std;

namespace NN {

    Neuron::Neuron(double bias) {
        this->bias = bias;
    }

    double Neuron::calculateOutput(vector<double> inputs) {
        this->inputs = inputs;
        this->totalNetInput = inner_product(this->inputs.begin(), this->inputs.end(), this->weights.begin(), 0.0)
                              + this->bias;
        this->output = 1.0 / (1.0 + exp(-this->totalNetInput));
        return this->output;
    }

    double Neuron::calculateError(double desiredOutput) {
        return 0.5 * (desiredOutput - this->output) * (desiredOutput - this->output);
    }

    double Neuron::calculatePdErrorWrtTotalNetInput(double desiredOutput) {
        return this->calculatePdErrorWrtOutput(desiredOutput) * this->calculatePdTotalNetInputWrtInput();
    }

    double Neuron::calculatePdErrorWrtOutput(double desiredOutput) {
        return -(desiredOutput - this->output);
    }

    double Neuron::calculatePdTotalNetInputWrtInput() {
        return this->output * (1.0 - this->output);
    }

    double Neuron::calculatePdTotalNetInputWrtWeight(int index) {
        return this->inputs[index];
    }

    Neuron::~Neuron() {}

}
